import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  input1 = true;
  input2 = 'bbb';
  input3 = '<h1>aaa</h1>';

  output1 = '';
  output2 = '';
  output3 = '';

  setInput2(event: Event) {
    const value = (event.target as HTMLInputElement).value;
    this.input2 = value;
  }

  clickOutput() {
    this.output1 = this.input1 ? 'チェックあり' : 'チェックなし';
    this.output2 = this.input2;
    this.output3 = this.input3;
  }

  clickClear() {
    this.input1 = false;
    this.input2 = '';
    this.input3 = '';

    this.output1 = '';
    this.output2 = '';
    this.output3 = '';
  }
}
