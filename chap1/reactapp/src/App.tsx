import { useState } from "react";
import "./App.css";

const App = () => {
  const [input1, setInput1] = useState(true);
  const [input2, setInput2] = useState("aaa");
  const [input3, setInput3] = useState("<h1>aaa</h1>");

  const [output1, setoutput1] = useState("");
  const [output2, setoutput2] = useState("");
  const [output3, setoutput3] = useState("");

  const clickOutput = () => {
    setoutput1(input1 ? "チェックあり" : "チェックなし");
    setoutput2(input2);
    setoutput3(input3);
  };

  const clickClear = () => {
    setInput1(false);
    setInput2("");
    setInput3("");
    setoutput1("");
    setoutput2("");
    setoutput3("");
  };

  return (
    <div className="App">
      <section>
        <h3>入力</h3>
        <div className="block">
          <label htmlFor="input1"> チェック </label>
          <input
            type="checkbox"
            name="input1"
            id="input1"
            checked={input1}
            onChange={(e) => setInput1(e.target.checked)}
          />
        </div>
        <div className="block">
          <label htmlFor="input2"> 値入力1 </label>
          <input
            type="text"
            name="input2"
            id="input2"
            value={input2}
            onChange={(e) => setInput2(e.target.value)}
          />
        </div>
        <div className="block">
          <label htmlFor="input3"> 値入力2(HTML) </label>
          <input
            type="text"
            name="input3"
            id="input3"
            value={input3}
            onChange={(e) => setInput3(e.target.value)}
          />
        </div>
      </section>

      <section>
        <h3>出力</h3>
        <div className="block">
          <span>チェック</span>
          <span>{output1}</span>
        </div>
        <div className="block">
          <span>値出力1</span>
          <span>{output2}</span>
        </div>
        <div className="block">
          <span>値出力2(HTML)</span>
          <span
            dangerouslySetInnerHTML={{
              __html: output3,
            }}
          ></span>
        </div>
      </section>

      <div className="block-button">
        <input type="button" id="out" value="出力" onClick={clickOutput} />
        <input type="button" id="clear" value="クリア" onClick={clickClear} />
      </div>
    </div>
  );
};

export default App;
