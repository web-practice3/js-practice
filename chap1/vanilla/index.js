// 入力要素
let input1 = document.getElementById("input1")
let input2 = document.getElementById("input2")
let input3 = document.getElementById("input3")
input3.value = "<h1>aaa</h1>"

// 出力要素
let output1 = document.querySelector("#output1")
let output2 = document.querySelector("#output2")
let output3 = document.querySelector("#output3")

// 出力ボタン
let out = document.querySelector("#out")
out.addEventListener('click', () => {
    output1.innerText = input1.checked ? "チェックあり" : "チェックなし"
    output2.innerText = input2.value
    output3.innerHTML = input3.value
})

// クリアボタン
let clear = document.querySelector("#clear")
clear.addEventListener('click', () => {
    input1.checked = false
    input2.value = ""
    input3.value = ""

    output1.innerText = ""
    output2.innerText = ""
    output3.innerHTML = ""
})