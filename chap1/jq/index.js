$('#input3').val("<h1>aaa</h1>")

// 出力ボタンをクリックした時
$("#out").click(() => {
    const checkText = $("#input1").prop('checked') ? "チェックあり" : "チェックなし"
    $("#output1").text(checkText);
    $("#output2").text($("#input2").val());
    $("#output3").html($("#input3").val());
});

// クリアボタンをクリックした時
$("#clear").click(() => {
    $("#input1").prop('checked', false)
    $("#input2").val('')
    $("#input3").val('')

    $("#output1").text("")
    $("#output2").text("")
    $("#output3").html("")
})