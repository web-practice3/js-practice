# Js Practice

## Description

JavaScript演習問題の解答集。
各設問をフレームワークごとに解いていく。

- Vanilla JS
- JQuery
- Angular
- Vue.js
- React

## Usage

### Angular

- Angular CLI のインストール
```
npm install -g @angular/cli 
```

- 依存関係のインストール
```
cd path/to/angular
npm install
```

- アプリケーション起動
```
npm start -- --open
```

### Vue

- Vue CLI のインストール
```
npm install -g @vue/cli
```

- 依存関係のインストール
```
cd path/to/vue
npm install
```

- アプリケーション起動
```
npm run serve -- --open
```

### React

- 依存関係のインストール
```
cd path/to/reactapp
npm install
```

- アプリケーション起動
```
npm start
```